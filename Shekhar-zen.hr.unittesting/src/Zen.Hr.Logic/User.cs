﻿namespace Zen.Hr.Logic
{
    public class User
    {
        public int UserID { get; set; }
        public string UserType { get; set; }
        public bool IsActive { get; set; }
    }
}