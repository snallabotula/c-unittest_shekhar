﻿namespace Zen.Hr.Logic
{
    public interface IUserDataAccess
    {
        User[] GetAllUsers();
        User[] GetAllActiveUsers();
    }
    public class AccessUsers : IUserDataAccess
    {
        public AccessUsers()
        {
        }
        //Temporary user data manually created
        public User[] GetAllUsers()
        {
            User allUsers = new User();
            allUsers.UserID = 1;
            allUsers.UserType = "Active";
            allUsers.IsActive = true;

            User inActiveusers = new User();
            inActiveusers.UserID = 2;
            inActiveusers.UserType = "InActive";
            inActiveusers.IsActive = false;

            return new User[2] { allUsers, inActiveusers };
        }

        public User[] GetAllActiveUsers()
        {
            // Users has to be read from database. but delcaring locally

            User allUsers = new User();
            allUsers.UserID = 1;
            allUsers.UserType = "Active";
            allUsers.IsActive = true;

            return new User[1] { allUsers };
        }
    }
}