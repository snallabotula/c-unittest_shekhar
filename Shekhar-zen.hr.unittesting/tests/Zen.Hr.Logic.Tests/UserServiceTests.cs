﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Reflection;

namespace Zen.Hr.Logic.Tests
{
    [TestClass]
    public class UserServiceTests
    {
        private UserService concern;
        //
        [TestMethod]
        public void Successfully_Returns_All_Users_When_Passing_In_False()
        {
            try
            {
                User allUsers = new User();
                allUsers.UserID = 1;
                allUsers.UserType = "Active";
                allUsers.IsActive = true;
                User inActiveusers = new User();
                inActiveusers.UserID = 2;
                inActiveusers.UserType = "InActive";
                inActiveusers.IsActive = false;

                User[] ExpectedUsers = new User[2] { allUsers, inActiveusers };

                AccessUsers objAccessusers = new AccessUsers();
                concern = new UserService(objAccessusers);
                User[] totalUsers = concern.GetUsers(false);

                Assert.IsNotNull(totalUsers, "User object is empty");
                Assert.IsTrue(totalUsers.Length > 0, "user count is empty");             

            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        [TestMethod]
        public void Successfully_Returns_All_Active_Users_When_Passing_In_True()
        {
            try
            {
                User allUsers = new User();
                allUsers.UserID = 1;
                allUsers.UserType = "Active";
                allUsers.IsActive = true;

                User[] allActiveUsers = new User[1] { allUsers };

                AccessUsers objAccessUsers = new AccessUsers();
                concern = new UserService(objAccessUsers);
                User[] allExpectedUsers = concern.GetUsers(true);

                Assert.IsNotNull(allActiveUsers, "User object is empty");
                Assert.IsTrue(allExpectedUsers.Length > 0, "Active user count is empty");
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}
